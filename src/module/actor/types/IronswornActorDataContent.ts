export default interface IronswornActorDataContent {
  health: number;
  momentum: {
    value: number;
    reset: number;
    max: number;
  };
  spirit: number;
  supply: number;
  attributes: {
    edge: number;
    heart: number;
    iron: number;
    shadow: number;
  };
  debilities: {
    banes: {
      corrupted: boolean;
      maimed: boolean;
    };
    burdens: {
      cursed: boolean;
      tormented: boolean;
    };
    conditions: {
      encumbered: boolean;
      shaken: boolean;
      unprepared: boolean;
      wounded: boolean;
    };
  };
}

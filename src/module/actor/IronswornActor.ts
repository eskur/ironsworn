import IronswornActorData from './types/IronswornActorData';
import IronswornActorDataContent from './types/IronswornActorDataContent';

export default class IronswornActor extends Actor {
  /**
   * Extends data from base Actor class
   */
  prepareData() {
    super.prepareData();
    const actorData = this.data as IronswornActorData;
    const { data } = actorData;

    IronswornActor.preparePlayerAttributes(data);
    return this.data;
  }

  private static preparePlayerAttributes(data: IronswornActorDataContent) {
    const { attributes, debilities, health, momentum, spirit, supply } = data;
    attributes.edge = 3;
    attributes.iron = 3;
    console.log('****************************');
    console.log(`Actor data ${JSON.stringify(data)}`);
  }
}

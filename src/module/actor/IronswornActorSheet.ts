export default class IronswornActorSheet extends ActorSheet {
  templatePath = 'systems/ironsworn/templates/IronswornCharacterSheet.html';

  /**
   * @override
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
  static get defaultOptions(): object {
    const objectReturn = mergeObject(super.defaultOptions, {
      classes: ['ironsworn', 'sheet', 'character'],
      width: 800,
      height: 780,
      tabs: [{ navSelector: '.sheet-tabs', contentSelector: '.sheet-body', initial: 'moves' }],
    });
    return objectReturn;
  }

  get template(): string {
    // Later you might want to return a different template
    // based on user permissions.
    return this.templatePath;
  }
}

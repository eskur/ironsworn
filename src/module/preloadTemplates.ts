const preloadTemplates = async () => {
  const templatePaths = [
    // Add paths to "systems/Ironsworn/templates"
  ];

  return loadTemplates(templatePaths);
};

export default preloadTemplates;

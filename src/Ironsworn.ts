/**
 * This is your TypeScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your system, or remove it.
 * Author: [your name]
 * Content License: [copyright and-or license] If using an existing system
 * 					you may want to put a (link to a) license or copyright
 * 					notice here (e.g. the OGL).
 * Software License: [your license] Put your desired license here, which
 * 					 determines how others may use and modify your system
 */

// Import TypeScript modules
import registerSettings from './module/settings';
import preloadTemplates from './module/preloadTemplates';
import IRONSWORN from './module/config';
import IronswornActor from './module/actor/IronswornActor';
import IronswornActorSheet from './module/actor/IronswornActorSheet';

/* ------------------------------------ */
/* Initialize system					*/
/* ------------------------------------ */
Hooks.once('init', async function init() {
  console.log('Ironsworn | Initializing Ironsworn');

  // Assign custom classes and constants here
  // Debug
  CONFIG.debug.hooks = true;
  // Record Configuration Value
  CONFIG.IRONSWORN = IRONSWORN;

  CONFIG.Actor.entityClass = IronswornActor;

  // Register custom system settings
  registerSettings();

  // Register Actor sheets
  Actors.unregisterSheet('core', ActorSheet); // remove the default actor sheet
  Actors.registerSheet('ironsworn', IronswornActorSheet, {
    // types: 'player',
    makeDefault: true,
  });

  // Preload Handlebars templates
  await preloadTemplates();
});

/* ------------------------------------ */
/* Setup system							*/
/* ------------------------------------ */
Hooks.once('setup', function setup() {
  // Do anything after initialization but before
  // ready
});

/* ------------------------------------ */
/* When ready							*/
/* ------------------------------------ */
Hooks.once('ready', function ready() {
  // Do anything once the system is ready
});

// Add any additional hooks if necessary
